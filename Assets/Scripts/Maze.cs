﻿using UnityEngine;
using UnityEngine.Serialization;

public class Maze : MonoBehaviour
{
    [System.Serializable]
    public class Cell
    {
        public bool Visited = false;
        public GameObject north, south, east, west;
    }

    public GameObject Wall;
    public int XSize = 5;
    public int YSize = 5;
    public float WallLength = 1f;
    private Cell[] _cells;
    private Vector3 _initialPos;
    GameObject _wallHolder;
    [SerializeField] private int _currentCell = 0;
    private int _totalCells;

    private void Start()
    {
        CreateWalls();
        CreateCells();
        CreateMaze();
    }

    private void CreateWalls()
    {
        _wallHolder = new GameObject {name = "wallHolder"};
        _initialPos = new Vector3((-XSize / 2f) + WallLength / 2, 0, (-YSize / 2) + WallLength / 2);
        Vector3 myPos = _initialPos;
        GameObject tempWall;
        // For X axis
        for (int i = 0; i < YSize; i++)
        {
            for (int j = 0; j <= XSize; j++)
            {
                myPos = new Vector3(_initialPos.x + j * WallLength - WallLength / 2, 0,
                    _initialPos.z + i * WallLength - WallLength / 2);
                tempWall = Instantiate(Wall, myPos, Quaternion.identity);
                tempWall.transform.parent = _wallHolder.transform;
            }
        }

        //Y axis
        for (int i = 0; i <= YSize; i++)
        {
            for (int j = 0; j < XSize; j++)
            {
                myPos = new Vector3(_initialPos.x + j * WallLength, 0,
                    _initialPos.z + i * WallLength - WallLength);
                tempWall = Instantiate(Wall, myPos, Quaternion.Euler(0, 90f, 0));
                tempWall.transform.parent = _wallHolder.transform;
            }
        }
    }

    private void CreateCells()
    {
        int children = _wallHolder.transform.childCount,
            eastWestProcess = 0,
            termCount = 0,
            childProcess = 0;
        GameObject[] allWalls = new GameObject[children];
        _totalCells = XSize * YSize;
        _cells = new Cell[_totalCells];
        //Get all children
        for (int i = 0; i < children; i++)
            allWalls[i] = _wallHolder.transform.GetChild(i).gameObject;
        //Assigns walls to cells
        for (int i = 0; i < _cells.Length; i++)
        {
            _cells[i] = new Cell();
            _cells[i].east = allWalls[eastWestProcess];
            _cells[i].south = allWalls[i + (XSize + 1) * YSize];
            if (termCount == XSize)
            {
                eastWestProcess += 2;
                termCount = 0;
            }
            else
            {
                eastWestProcess++;
            }

            termCount++;
            childProcess++;
            _cells[i].west = allWalls[eastWestProcess];
            _cells[i].north = allWalls[childProcess + (XSize + 1) * YSize + XSize - 1];
        }
    }

    private void CreateMaze()
    {
        GetNeighbour();
    }

    private void GetNeighbour()
    {
        int length = 0;
        int[] neighbours = new int[4];
        int check = (_currentCell + 1) / XSize;
        check -= 1;
        check *= XSize;
        check += XSize;
        
        //north
        if (_currentCell + XSize < _totalCells)
        {
            if (_cells[_currentCell - 1].Visited == false)
            {
                neighbours[length] = _currentCell + XSize;
                length++;
            }
        }

        //east
        if (_currentCell - 1 >= 0 && _currentCell != check)
        {
            if (_cells[_currentCell - 1].Visited == false)
            {
                neighbours[length] = _currentCell - 1;
                length++;
            }
        }

        //south
        if (_currentCell - XSize >= 0)
        {
            if (_cells[_currentCell - 1].Visited == false)
            {
                neighbours[length] = _currentCell - XSize;
                length++;
            }
        }

        //west
        if (_currentCell + 1 < _totalCells && (_currentCell + 1) != check)
        {
            if (_cells[_currentCell + 1].Visited == false)
            {
                neighbours[length] = _currentCell + 1;
                length++;
            }
        }

        for (int i = 0; i < 4; i++)
        {
            print(neighbours[i]);
        }
    }
}